#!/bin/bash

URL=$URL

echo "URL : $URL";

counterTest=$(curl -sb -H "$URL/count" | jq -r '.pingCount');
counter="0";

echo "Test de la requete count :"

if [ "$counter" = "$counterTest" ]
then
	echo "TEST COUNTER : le compteur est initialisé à 0. ("$counterTest"/"$counter")";
else
	echo "TEST COUNTER : Le compteur ne s'est pas initialisé. ("$counterTest" au lieu de "$counter")";
fi


pong=$(curl -sb -H "$URL/ping" | jq -r '.message');

pongTest="pong";
echo "Test de la requete ping :"
if [ "$pong" = "$pongTest" ]
then
	echo "TEST PING : Pong s'affiche."
else
	echo "TEST PING : Pong ne s'affiche pas."
fi

echo "TEST : LANCEMENT DE 2 PINGS SUPPLEMENTAIRES (Pour un total de 3)"
curl -sb -H "$URL/ping"
curl -sb -H "$URL/ping"


counterTest=$(curl -sb -H "$URL/count" | jq -r '.pingCount');
counter="3";

if [ "$counter" = "$counterTest" ]
then
	echo "TEST COUNTER : le compteur s'incrémente à chaque appel. ("$counterTest"/"$counter")";
else
	echo "TEST COUNTER : Le compteur ne s'incrémente pas. ("$counterTest"/"$counter")";
fi
